//
//  HWMatrixViewController.h
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HWHomeworkChildController.h"

@protocol HWMatrixViewControllerDelegate;

@interface HWMatrixViewController : UIViewController <HWHomeworkChildController>

@property (nonatomic, weak) id<HWMatrixViewControllerDelegate> delegate;

- (void)setMatrix:(NSArray<NSArray<NSNumber *> *> *)matrix;

@end

@protocol HWMatrixViewControllerDelegate <NSObject>

@required
- (void)matrixController:(HWMatrixViewController *)controller needCreateMatrixWithRowsCount:(NSInteger)rowsCount columnsCount:(NSInteger)columnsCount;

@end
